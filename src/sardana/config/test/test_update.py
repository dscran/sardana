# -*- coding: utf-8 -*-

##############################################################################
##
## This file is part of Sardana
##
## http://www.tango-controls.org/static/sardana/latest/doc/html/index.html
##
## Copyright 2019 CELLS / ALBA Synchrotron, Bellaterra, Spain
##
## Sardana is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Sardana is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with Sardana.  If not, see <http://www.gnu.org/licenses/>.
##
##############################################################################

import io

from ruamel.yaml import YAML

from ..update import update_config


def test_update_basic(sar_demo_yaml_raw, sar_demo_yaml):
    merged = update_config(sar_demo_yaml_raw, sar_demo_yaml)
    ruamel = YAML(typ="rt")
    out = io.StringIO()
    ruamel.dump(merged, out)
    assert sar_demo_yaml_raw == out.getvalue()
